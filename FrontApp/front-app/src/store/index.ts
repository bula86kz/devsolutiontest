import { createStore, Commit } from 'vuex'

export default createStore({
  state: {
    authenticated : false
  },
  getters: {
  },
  mutations: {
    SET_AUTH: (state: {authenticated: boolean}, isAuthenticated: boolean) => state.authenticated = isAuthenticated
  },
  actions: {
    setAuth: ({commit}: {commit: Commit}, isAuthenticated: boolean) => commit("SET_AUTH", isAuthenticated)
  },
  modules: {
  }
})
