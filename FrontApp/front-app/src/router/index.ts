import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomePage from "@/views/Home.vue";
import LoginPage from "@/views/Login.vue";
import RegisterPage from "@/views/Register.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path : "/",
    component : HomePage 
  },
  {
    path : "/login",
    component : LoginPage 
  },
  {
    path : "/register",
    component : RegisterPage 
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
