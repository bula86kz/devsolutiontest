﻿using System;
using System.Text.Json.Serialization;

namespace AuthorizationApp.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string TelephoneNumber { get; set; }
        public DateTime BirthDate { get; set; }
        
        [JsonIgnore]
        public string Password { get; set; }
    }
}