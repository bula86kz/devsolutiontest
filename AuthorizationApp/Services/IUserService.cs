﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AuthorizationApp.DTOs;
using AuthorizationApp.Models;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationApp.Services
{
    public interface IUserService
    {
        Task CreateAsync(UserDTO userDTO);
        Task<List<User>> GetAllUsersAsync();
        Task<User> GetUserByIdAsync(int id);
        Task<User> GetUserByEmailAsync(string email);
        Task<User> UpdateUserByIdAsync(int id, UserDTO userDto);
        Task DeleteUserByIdAsync(int id);
    }
}