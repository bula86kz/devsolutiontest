﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AuthorizationApp.DTOs;
using AuthorizationApp.Models;
using AuthorizationApp.Repositories;

namespace AuthorizationApp.Services
{
    public class UserServiceImpl : IUserService
    {
        private readonly IUserRepository _repository;

        public UserServiceImpl(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task CreateAsync(UserDTO userDTO)
        {
            User user = new User()
            {
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName,
                Email = userDTO.Email,
                TelephoneNumber = userDTO.TelephoneNumber,
                BirthDate = userDTO.BirthDate,
                Id = userDTO.Id,
                Password = BCrypt.Net.BCrypt.HashPassword(userDTO.Password)
            };
            
            await _repository.CreateAsync(user);
        }

        public Task<List<User>> GetAllUsersAsync()
        {
            return _repository.GetAllUsers();
        }

        public Task<User> GetUserByIdAsync(int id)
        {
            return _repository.GetUserByIdAsync(id);
        }

        public Task<User> GetUserByEmailAsync(string email)
        {
            return _repository.GetUserByEmailAsync(email);
        }

        public Task<User> UpdateUserByIdAsync(int id, UserDTO userDto)
        {
            return _repository.UpdateUserById(id, userDto);
        }

        public Task DeleteUserByIdAsync(int id)
        {
            return _repository.DeleteUserById(id);
        }
    }
}