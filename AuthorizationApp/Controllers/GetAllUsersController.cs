﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AuthorizationApp.Models;
using AuthorizationApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationApp.Controllers
{
    [Route("api")]
    [ApiController]
    public class GetAllUsersController : Controller
    {
        private readonly IUserService _service;

        public GetAllUsersController(IUserService service)
        {
            _service = service;
        }

        [HttpGet("users")]
        public async Task<List<User>> GetAllUsers()
        {
            return await _service.GetAllUsersAsync();
        }
    }
}