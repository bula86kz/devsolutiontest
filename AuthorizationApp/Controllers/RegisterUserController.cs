﻿using System.Threading.Tasks;
using AuthorizationApp.DTOs;
using AuthorizationApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationApp.Controllers
{
    [Route("api")]
    [ApiController]
    public class CreateUserController : Controller
    {
        private readonly IUserService _userService;

        public CreateUserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterUser([FromBody] UserDTO userDto)
        {
            await _userService.CreateAsync(userDto);
            return Ok();
        }
    }
}