﻿using System.Threading.Tasks;
using AuthorizationApp.Models;
using AuthorizationApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationApp.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class GetUserByIdController : Controller
    {
        private readonly IUserService _service;

        public GetUserByIdController(IUserService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        public Task<User> GetUserById(int id)
        {
            return _service.GetUserByIdAsync(id);
        }
    }
}