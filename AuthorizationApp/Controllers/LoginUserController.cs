﻿using System.Threading.Tasks;
using AuthorizationApp.DTOs;
using AuthorizationApp.Models;
using AuthorizationApp.Services;
using AuthorizationApp.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationApp.Controllers
{
    [Route("api")]
    [ApiController]
    public class LoginUserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IJwtService _jwtService;

        public LoginUserController(IUserService userService, IJwtService jwtService)
        {
            _userService = userService;
            _jwtService = jwtService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginUser(LoginDTO loginData)
        {
            User user = await _userService.GetUserByEmailAsync(loginData.Email);

            if (user is null)
            {
                return BadRequest(new { message = "Неверные данные" });
            }

            if (!BCrypt.Net.BCrypt.Verify(loginData.Password, user.Password))
            {
                return BadRequest(new { message = "Неверные данные" });
            }

            var jwt = _jwtService.GenerateJwt(user.Id);
            Response.Cookies.Append("jwt", jwt, new CookieOptions
            {
                HttpOnly = true
            });
            
            return Ok();
        }
    }
}