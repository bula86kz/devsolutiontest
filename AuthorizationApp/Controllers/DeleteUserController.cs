﻿using System.Threading.Tasks;
using AuthorizationApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationApp.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class DeleteUserById
    {
        private readonly IUserService _service;

        public DeleteUserById(IUserService service)
        {
            _service = service;
        }

        [HttpDelete("{id}")]
        public Task DeleteUserId(int id)
        {
            return _service.DeleteUserByIdAsync(id);
        }
    }
}