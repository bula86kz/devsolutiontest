﻿using System;
using System.Threading.Tasks;
using AuthorizationApp.Models;
using AuthorizationApp.Services;
using AuthorizationApp.Utils;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationApp.Controllers
{
    [Route("api")]
    [ApiController]
    public class GetUserController : Controller
    {
        private readonly IUserService _service;
        private readonly IJwtService _jwtService;

        public GetUserController(IUserService service, IJwtService jwtService)
        {
            _service = service;
            _jwtService = jwtService;
        }

        [HttpGet("user")]
        public async Task<ActionResult<User>> GetAuthUser()
        {
            try
            {
                var jwt = Request.Cookies["jwt"];
                var token = _jwtService.VerifyJwt(jwt);
                int userId = int.Parse(token.Issuer);
                var user = await _service.GetUserByIdAsync(userId);
                return user;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest((new { message = "Пользователь не авторизован" }));
            }
        }
    }
}