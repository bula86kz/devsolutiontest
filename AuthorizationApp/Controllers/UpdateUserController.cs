﻿using System.Threading.Tasks;
using AuthorizationApp.DTOs;
using AuthorizationApp.Models;
using AuthorizationApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationApp.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UpdateUserController : Controller
    {
        private readonly IUserService _service;

        public UpdateUserController(IUserService service)
        {
            _service = service;
        }

        [HttpPut("{id}")]
        public Task<User> UpdateUserById(int id, UserDTO userDto)
        {
            return _service.UpdateUserByIdAsync(id, userDto);
        }
    }
}