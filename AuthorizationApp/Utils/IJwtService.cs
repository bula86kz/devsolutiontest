﻿using System.IdentityModel.Tokens.Jwt;

namespace AuthorizationApp.Utils
{
    public interface IJwtService
    {
        string GenerateJwt(int id);
        JwtSecurityToken VerifyJwt(string jwt);
    }
}