﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AuthorizationApp.DTOs;
using AuthorizationApp.Models;

namespace AuthorizationApp.Repositories
{
    public interface IUserRepository
    {
        Task CreateAsync(User user);
        Task<List<User>> GetAllUsers();
        Task<User> GetUserByIdAsync(int id);
        Task<User> GetUserByEmailAsync(string email);

        Task<User> UpdateUserById(int id, UserDTO userDto);
        Task DeleteUserById(int id);
    }
}