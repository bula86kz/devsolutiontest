﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthorizationApp.Data;
using AuthorizationApp.DTOs;
using AuthorizationApp.Models;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationApp.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserContext _userContext;

        public UserRepository(UserContext userContext)
        {
            _userContext = userContext;
        }

        public Task CreateAsync(User user)
        {
            _userContext.Users.Add(user);
            user.Id = _userContext.SaveChanges();

            return _userContext.Users.FirstOrDefaultAsync(u => u.Id == user.Id);
        }

        public Task<List<User>> GetAllUsers()
        {
            return _userContext.Users.Where(u => true).ToListAsync();
        }

        public Task<User> GetUserByIdAsync(int id)
        {
            return _userContext.Users.FirstOrDefaultAsync(user => user.Id == id);
        }

        public Task<User> GetUserByEmailAsync(string email)
        {
            return  _userContext.Users.FirstOrDefaultAsync(user => user.Email.Equals(email));
        }

        public Task<User> UpdateUserById(int id, UserDTO userDto)
        {
            var user = _userContext.Users.FirstOrDefault(user => user.Id == id);

            if (user != null)
            {
                user.FirstName = userDto.FirstName;
                user.LastName = userDto.LastName;
                user.Email = userDto.Email;
                user.TelephoneNumber = userDto.TelephoneNumber;
                user.BirthDate = userDto.BirthDate;
                user.Id = id;
                user.Password = BCrypt.Net.BCrypt.HashPassword(userDto.Password);
                _userContext.SaveChanges();
                return _userContext.Users.FirstOrDefaultAsync(u => u.Id == id);
            }

            throw new ArgumentNullException(nameof(userDto));
        }

        public Task DeleteUserById(int id)
        {
            var userToDelete = _userContext.Users.FirstOrDefault(u => u.Id == id);
            if (userToDelete != null) _userContext.Remove(userToDelete);
            return _userContext.SaveChangesAsync();
        }
    }
}