# DevSolutionTest


## Подготовка

1. Для запуска бэкенда нужно использовать SDK C# .Net v3.1;
2. В проекте использовалась:
	- СУБД PostgreSQL;
	- Microsoft.EntityFrameworkCore 3.1.27;
	- Microsoft.EntityFrameworkCore.Design 3.1.27;
	- Npgsql.EntityFrameworkCore.PostgreSQL 3.1.18;
	- BCrypt.Net-Next 4.0.3;
	- Microsoft.AspNetCore.Cors 2.2;
	- System.IdentityModel.Tokens.Jwt 6.21.
3. Нужно в настройках appsettings.json в объекте ConnectionStrings (Default) указать свои настройки БД;
4. Необходимо создать свою миграцию с помощью "dotnet ef migrations add <name>" и dotnet ef database update;
5. С помощью формы регистрации заполнить данные.